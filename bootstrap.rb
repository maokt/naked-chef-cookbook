#!/usr/bin/ruby
#
# This is both a bootstrap script to run chef-client and the config file for chef-client during that bootstrap.
#

my_path = File.expand_path(__FILE__)
my_dir = File.dirname(my_path)
my_name = File.basename(my_dir)
my_cookbook_path = File.absolute_path("..", my_dir)
my_basename = File.basename(my_cookbook_path)

abort %Q(FAIL: expecting to be in a "cookbooks" directory, not "#{my_basename}".) unless my_basename == "cookbooks"

if __FILE__ == $0
    # Running as the bootstrap script.
    run_list = [ my_name ].concat(ARGV.select { |n| Dir.exists?(File.absolute_path(n, my_cookbook_path)) })
    exec "chef-client", "-c", my_path, "-r", run_list.map{ |n| "recipe[#{n}]" }.join(",")
else
    # Running as the config file.
    # Sorry, need to use a global variable to set the node attribute during bootstrap.
    $naked_cookbook_path = my_cookbook_path
    cookbook_path my_cookbook_path
    cache_path "/var/chef"
    local_mode true
end
