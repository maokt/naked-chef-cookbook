if defined?($naked_cookbook_path)
    node.normal[:naked][:cookbook_path] = $naked_cookbook_path
end

directory "/etc/chef" do
    owner "root"
    group "root"
    mode 00755
    action :create
end

template "/etc/chef/client.rb" do
    source "client.erb"
    mode "0644"
    variables({
        :cookbook_path => node[:naked][:cookbook_path],
    })
end

service "chef-client" do
    # For now, don't run chef-client by default.
    action :disable
end

