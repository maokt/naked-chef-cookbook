The Naked Chef Cookbook
=======================

I wanted a simple recipe to bootstrap a Chef to run in local mode, without
a Chef server. I thought that finding a name would be difficult, then
I spotted a Jamie Oliver book on the shelf... and it seemed to fit.

This cookbook will setup /etc/chef/client.rb to run chef-client in local mode
and use the cookbooks directory where this cookbook exists.

Usage
-----

Clone this repo into your cookbooks directory and then run bootstrap.rb (or if
you use the Chef Dev Kit, run /opt/chefdk/bin/chef exec ruby bootstrap.rb).

Not root?
---------

Chef client runs as root to manage the host. In local mode it will create the
node file as root. I want to be able to use knife in local mode, as non-root,
to modify the run lists, so before running the bootstrap.rb I set a default
acl on the nodes directory to make sure I always get write access:

    setfacl -d -m u:marty:rwx nodes

